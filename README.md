# PyConFr Mastodon Bot

## Ideas for 2024+

Don't shoot them all for a whole day, it's too spammy.

Maybe toot like:

- 1/5 of the talks 8 days before
- 1/5 of the talks 7 days before
- 1/5 of the talks 6 days before
- ...

or any variation of those numbers.


## Fetching the talks

PonyConf don't have an API so to fetch the talks data, run:

    $ rsync root@deb2.afpy.org:/home/ponyconf/src/db.sqlite3 ./


## Dry run

To test the pyconfr-to-mastodon bot, just use `--dry-run`, it won't connect to Mastodon.

Passing a `client_id`, `client_secret`, and `access_token` is mandatory though, so one can use:

    $ python pyconfr-to-mastodon.py _ _ _ --dry-run


To avoid boring "sleeps", just tell it to toot in the past, like `--ahead-days 999`.


## Real use

Combined with `pass` one can use it like;

    $ python pyconfr-to-mastodon.py --ahead-days 7 "$(pass afpy_mamot_client_id)" "$(pass afpy_mamot_client_secret)" "$(pass afpy_mamot_access_token)"
